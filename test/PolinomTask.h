﻿#ifndef POLINOMTASK_H
#define POLINOMTASK_H

#include "TestFunc.h"

class PolinomTask : public TestFunc {
	double parR[10], par{ 1.0 }, sign{ +1.0 };
	int rootCount;
public:
	PolinomTask(double aNew, double bNew, int _rootCount, double a = 0.0, double b = 1.0)
		: TestFunc(aNew, bNew, a, b), rootCount(_rootCount)
	{  // [a, b] - "Родные" границы изменения аргумента
		paramGeneration();
		scanTestFunction();
	}
	virtual	void paramGeneration()
	{
		for (int i = 1; i < rootCount - 1; ++i)
		{
			parR[i] = getRandomIn(Fa + 0.005, Fb - 0.005);
			//@	  cout << "root = " << parR[i] << "; ";
		}
		parR[0] = Fa;  parR[rootCount - 1] = Fb;
		//@	cout << endl;
	}
	double calcTestFunc(double x)
	{
		auto PolinomFunc{ 1.0 };
		for (auto i = 0; i < rootCount; i++)
			PolinomFunc = PolinomFunc * (x - parR[i]);
		return PolinomFunc;
	}
};
#endif // POLINOMTASK_H
