﻿#include "pch.h"
#include <iostream>
#include "ParabolasMethod.h"
#include <locale>

int main()
{
		setlocale(LC_ALL, ".1251");
	//auto numtask = 2,//номер генерируемой задачи 
	//	constrcount = 4; //число функций-ограничений

	////=========== построение тестовой задачи из класса =======================
	//auto mu = 0.25; //желаемая относительная мера допустимого множества 
	//auto p = new TProblem(TypeProblem::TestClass, numtask);//создали объект тестовой задачи
	////устанавливаем параметры генерации задачи (вызов метода обязателен!):
	//p->setParamsTestTask(TestFuncType::Gibson,//тип целевой функции
	//	       TestFuncType::Sheckel,//тип функции первого ограничения (остальные -- полиномы)  
	//	        constrcount, mu);
	//p->generateTestTask();//выполняем генерацию задачи
	//auto munew = p->seachAlphaValue();//полученная относительная мера допустимого множества
	//
	//cout << "param alpha = " << p->getAlpha() //узнаем найденный уровень ограничения
	//	<< " munew = " << munew << endl;//печатаем относительную меру допустимого множества
	//========================================================================

	//============= получение конкретной предопределенной задачи из списка ====
	int numtask = 0, constrcount = 1;
	TProblem *p = new TProblem(TypeProblem::AnyTask, numtask);

	//вычисляем числа фибоначчи:
	TestFunc::generateFib();//требуется для работы p->scantesttask()

	//формируем список глобальных минимумов задачи:
	p->scanTestTask();

	cout << "\nminfunc = " << p->getMin() << " maxfunc = " << p->getMax() << endl;
	auto globmincount = p->getGlobalSolutionCount();//число глобальных минимумов:
	cout << "count global mins = " << globmincount << endl;
	for(auto i = 0; i < globmincount; ++i)//сами глобальные минимумы:
	 cout << "xmin = " << (*p)[i].x << " fmin = " << (*p)[i].y << endl;
	cout <<"------------------------------------------------------------"<<endl;

	const auto delta = 1.0e-10;
	const auto nu = 0.01;
	const auto betta = 0.2;
	const auto gamma = 1.2;
	const auto gamma_loc = 1;
	const auto max_steps = 300;
	const auto n_up = 150;

	auto tester = new parabolas_method(nu,betta,delta,gamma_loc,gamma, max_steps, n_up ,p);
	tester->run_method();
	tester->print_meas();


	delete p;
	delete tester;
	return 0;
}
