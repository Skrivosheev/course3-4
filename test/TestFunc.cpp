﻿#include "pch.h"
#include "TestFunc.h"
#include "TProblem.h"


//Временное хранилище грубых оценок локальных минимумов тестовой функции:
vector<TGlobalSolution> TestFunc::localTmpSolutions;
vector<unsigned int> TestFunc::Fib;

void TestFunc::scanTestFunction()
{
	localTmpSolutions.clear();
	globalSolutions.clear();
	double x, y, xNext, yNext, h = (FbNew - FaNew) / (pointsCount - 1);
	double yPred;
	yPred = (*this)(FaNew);
	minFunc = maxFunc = yPred;
	//@	cout << "-----------Set points:" << endl;
	//@	cout << "x = " << FaNew << " y = " << yPred << endl;
	x = FaNew + h;
	y = (*this)(x);
	if (y < minFunc) minFunc = y;
	if (y > maxFunc) maxFunc = y;
	//@	cout << "x = " << x << " y = " << y << endl;
	//Ищем все локальные "ямки"
	TGlobalSolution p;
	if (yPred < y)
	{
		p.x = FaNew; p.y = yPred;
		localTmpSolutions.push_back(p);
	}
	for (int i = 2; i <= pointsCount - 1; i++)
	{
		xNext = FaNew + h * i;
		yNext = (*this)(xNext);
		//@ cout << "x = " << xNext << " y = " << yNext << endl;
		if (yNext < minFunc) minFunc = y;
		if (yNext > maxFunc) maxFunc = y;
		if (yPred > y && y < yNext)
		{
			p.x = x; p.y = y;
			localTmpSolutions.push_back(p);
		}
		if (y > yNext && i == pointsCount - 1)
		{
			p.x = xNext; p.y = yNext;
			localTmpSolutions.push_back(p);
		}
		yPred = y; x = xNext; y = yNext;
	}
	//@	cout << "-----------Set local points:" << endl;
	//@	for (int j = 0; j < (int)localTmpSolutions.size(); ++j)
	//@		cout << "x = " << localTmpSolutions[j].x
	//@		<< " y = " << localTmpSolutions[j].y << endl;

	double porogFunc = minFunc + (maxFunc - minFunc) / 10;
	//Уточнение локальных минимумов, выделение глобальных:
	for (int i = 0; i < (int)localTmpSolutions.size(); ++i)
	{
		if (localTmpSolutions[i].y > porogFunc) continue;
		double x0, x1;
		x0 = localTmpSolutions[i].x - h;
		x1 = x0 + 2 * h;
		x0 = FaNew > x0 ? FaNew : x0;
		x1 = FbNew < x1 ? FbNew : x1;

		getLocalMin(x0, x1, (FbNew - FaNew)*epsX, this, nullptr, p);
		if (minFunc > p.y) minFunc = p.y;
		if (p.y < minFunc + epsY * (maxFunc - minFunc))
			globalSolutions.push_back(p);
	}

	int count = (int)globalSolutions.size(), minusCount = 0;
	for (int i = 0; i < (int)globalSolutions.size() - minusCount; ++i)
		if (globalSolutions[i].y > minFunc + epsY * (maxFunc - minFunc))
		{
			globalSolutions[i] = globalSolutions[count - 1 - minusCount];
			minusCount++;
		}
	globalSolutions.resize(count - minusCount);
}

//Определение максимального и минимального значений, 
// формирование списка точно найденных глобальных минимумов тестовой функции:
//Поиск минимума на [x0, x1] из [0, 1] методом Фибоначчи с абсолютной точностью epsXabs
// у функции (*obj)(x), при obj != NULL или (*obj2)(x) при obj2 != NULL;
// Результат помещается по ссылке в структуру p:

void TestFunc::getLocalMin(double x0, double x1, double epsXabs,
	TestFunc* obj, TProblem* obj2, TGlobalSolution & p)
{
	double delta = epsXabs;
	double eps = delta / 10000;
	int N = 2;
	while ((x1 - x0) / Fib[N] + eps > delta) ++N;
	double Lamda, a = x0, b = x1;
	Lamda = (double)Fib[N - 1] / (double)Fib[N];
	auto y = a + Lamda * (b - a), x = b - Lamda * (b - a);
	double Fx, Fy;
	if (obj) { Fx = (*obj)(x), Fy = (*obj)(y); }
	else if (obj2) { Fx = (*obj2)(x), Fy = (*obj2)(y); }
	auto stepsCount = 2;
	//@ cout << "a = " << a << "\t b = " << b << endl;
	while (stepsCount < N - 1)
	{
		++stepsCount;
		Lamda = (double)Fib[N - stepsCount + 1] / (double)Fib[N - stepsCount + 2];
		if (Fx < Fy)
		{
			b = y;
			y = x; Fy = Fx;
			x = b - Lamda * (b - a);
			if (obj) Fx = (*obj)(x);
			else if (obj2) Fx = (*obj2)(x);
		}
		else
		{
			a = x;
			x = y; Fx = Fy;
			y = a + Lamda * (b - a);
			if (obj) Fy = (*obj)(y);
			else if (obj2) Fy = (*obj2)(y);
		}
		//@ cout << "a = " << a << "\t b = " << b << endl;
	}
	// Проводим N-е измерение:
	if (Fx < Fy)
	{
		b = y;
		y = x; Fy = Fx;
		x = y - eps * (b - a);
		if (obj) Fx = (*obj)(x);
		else if (obj2) Fx = (*obj2)(x);
	}
	else
	{
		a = x;
		x = y; Fx = Fy;
		y = x + eps * (b - a);
		if (obj) Fy = (*obj)(y);
		else if (obj2) Fy = (*obj2)(y);
	}
	//@ cout << "a = " << a << "\t b = " << b << endl;
	//Последняя обработка, формируем решение:
	if (Fx < Fy) { p.x = x;  p.y = Fx; }
	else { p.x = y;  p.y = Fy; }
	//@ cout << "p.x = " << p.x << " p.y = " << p.y <<endl;
}