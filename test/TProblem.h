﻿#ifndef TPROBLEM_H
#define TPROBLEM_H

#include "TestFunc.h"
#include <algorithm>

class TProblem
{
	double Fa{ 0.0 }; double Fb{ 1.0 }; //Отрезок поиска
	TypeProblem typeProblem;//Тип задачи (из стандартного набора или тестового класса)
	int numberTask;//Номер задачи в стандартном наборе или номер задачи в тестовом классе
	int constraintsCount;//Количество ограничений
	vector<int> taskFunctions;//Номера функций, входящих в стандартную задачу
	TestFuncType typeMainConstraint, typeMinFunc;//Тип основ.ограничения и миним.функции
	vector<TestFunc*> testFunctions; //Набор функций тестовой задачи
	vector<double> constraintsValues;//Верхние ограничители функций тестовой задачи
	double alpha{ 0.98 }; //Относительный уровень отсечения всех ограничений
	double mu{ 0.25 }; //Желаемая доля допустимых точек
	int tmpPointCount{ 200 };//Размер сетки для оценивания меры допустимого множества

	int pointsCount{ 5000 };//Число точек в сетке при грубом определении Min и Max
	double epsX{ 1.0e-06 }; //Относит. точность определения глобальных минимумов по х
	double epsY{ 1.0e-04 }; //Относит. точность сравнения значений в глоб.минимумах
	vector<TGlobalSolution> globalSolutions; //Набор решений тестовой задачи
	double minFunc, maxFunc;//Границы колебания целевой функции на допустимом множестве
		//Временное хранилище грубых оценок условных локальных минимумов тестовой задачи
		// в нормированных координатах из [0, 1]:
	static vector<TGlobalSolution> localTmpSolutions;
	//Текущий условный локальный минимум (который должен уточняться)
	// в нормированных координатах из [0, 1]:
	TGlobalSolution anyLocalSolution;
public:
	TProblem(TypeProblem type = TypeProblem::AnyTask, int numTask = 1)
	{
		typeProblem = type;
		numberTask = numTask;
		if (type == TypeProblem::AnyTask)
		{
			initializeTask();//Заполняет поля описания задачи
		}
		else
		{ // constraintsCount = constrCount;
		 //  generateTestTask();//Генерация тестовой задачи
		}
	}
	~TProblem()
	{
		for (auto obj : testFunctions) delete obj;
		testFunctions.clear();
		taskFunctions.clear();
		globalSolutions.clear();
		constraintsValues.clear();

	}
	TProblem(const TProblem &) = delete;
	TProblem(const TProblem &&) = delete;
	TProblem & operator=(const TProblem&) = delete;
	TProblem & operator=(const TProblem&&) = delete;

	//Установка параметров генерации
	void setParamsTestTask(TestFuncType _typeMinFunc,
		TestFuncType _typeMainConstraint, int constrCount = 2, double _mu = 0.25)
	{
		typeMinFunc = _typeMinFunc;
		typeMainConstraint = _typeMainConstraint;
		constraintsCount = constrCount;
		alpha = 0.98;
		mu = _mu;
	}
	//Оператор вычисления функции с аргументом x из нормированного отрезка [0,1]
	// numFunc = 0 - целевая функция
	double operator()(double x, int numFunc)
	{
		numFunc = numFunc < 0 ? 0 : numFunc;
		numFunc = numFunc > constraintsCount ? constraintsCount : numFunc;
		auto xx = Fa + (Fb - Fa)*x;//Пересчет в реальные координаты
		if (typeProblem == TypeProblem::AnyTask)
			return anyFunction(xx, taskFunctions[numFunc]);
		return testFunction(xx, numFunc) - constraintsValues[numFunc];
	}
	//Вычисление функции обобщенного ограничения с аргументом x из нормир.отрезка [0, 1]
	double G(double x)
	{
		double gmax = (*this)(x, 1), g;
		for (auto i = 2; i <= constraintsCount; ++i)
		{
			g = (*this)(x, i);
			if (g > gmax) gmax = g;
		}
		return gmax;
	}
	//Оператор для вычисления в нормированных координатах вспомогательной функции F(x)
	// при уточнении условного локального минимума (вызывается из scanTestFunction()):
	double operator()(double x)
	{
		double g = G(x), F = max<double>(0.0, g);
		if (g <= 0) F += (*this)(x, 0);
		else F += max<double>((*this)(x, 0), anyLocalSolution.y);
		return F;
	}
	//Границы отрезка поиска:
	double left() { return Fa; }
	double right() { return Fb; }

	// Выч-е функции из встроенного набора в "родных" координатах:
	double anyFunction(double x, int numStFunc);
	// Вычисление функции сгенерированной тестовой задачи в "родных" координатах:
	double testFunction(double x, int numFunc)
	{
		numFunc = numFunc < 0 ? 0 : numFunc;
		numFunc = numFunc > (int)(this->testFunctions.size()) - 1 ? (int)this->testFunctions.size() - 1 : numFunc;
		return (*(this->testFunctions[numFunc]))(x);
	}

	void initializeTask(); //Инициализация встроенной одиночной задачи

	//Фабрика функций тестовой задачи:
	//Первый вызов должен быть при isConstraints == false
	// при isConstraints == false в поля Fa, Fb задачи должны помещаться значения
	// "родных" границ минимизируемой функции,
	// при последующих вызовах для генерации функций ограничений (при isConstraints == true)
	// конструкторы этих функций должны вызываться со значениями границ Fa, Fb,
	// взятых из полей генерируемой задачи
	TestFunc* generateTestFunction(TestFuncType funcType, bool isConstraints, int rootCount);

	void generateTestTask();//Генерация тестовой задачи
	int getConstraintsCount() //Возвращает количество ограничений
	{
		return constraintsCount;
	}
	//Возвращает значение верхнего ограничителя функции с номером numFunc для уровня alpha:
	double getConstraintValue(int numFunc, double alpha)
	{
		return testFunctions[numFunc]->getMin()
			+ (testFunctions[numFunc]->getMax() - testFunctions[numFunc]->getMin())*alpha;
	}
	//Обновление значения Alpha в экземпляре класса:
	void setNewAlphaValue(double newAlpha)
	{
		alpha = newAlpha;
		for (auto i = 1; i <= constraintsCount; ++i)
			constraintsValues[i] = getConstraintValue(i, alpha);
	}
	//Приближенное определение доли допустимых точек на сетке из M точек:
	double getProportionGoodDomain(int M);
	//Подбор уровня отсечения Alpha для достижения меры mu допустимого множества
	// (вернет достигнутое приближение mu)
	double seachAlphaValue();
	//Узнаем найденный относительный уровень отсечения для функций ограничений:
	double getAlpha() { return alpha; }
	//Определение максимального и минимального условных значений, 
	// формирование списка точно найденных условных глобальных минимумов тестовой задачи:
	void scanTestTask();
	double getMin() { return minFunc; }
	double getMax() { return maxFunc; }
	int getGlobalSolutionCount() //Количество глоб.минимумов тестовой задачи 
	{
		return (int)globalSolutions.size();
	}
	//Получение данных об i-м глобальном минимуме задачи:
	TGlobalSolution operator[](int i)
	{
		i = i < 0 ? 0 : i;
		i = (int)((size_t)i >= globalSolutions.size() ? globalSolutions.size() - 1 : i);
		return globalSolutions[i];
	}
	TypeProblem getTypeProblem() { return typeProblem; }
	int getTaskNumber() { return numberTask; }
};
#endif // TPROBLEM_H
