﻿#ifndef PINTERTASK_H
#define PINTERTASK_H

#include "TestFunc.h"

class PinterTask : public TestFunc {
	double xGlobMin; //Безусловный глобальный минимум
public:
	PinterTask(double aNew, double bNew, double a = -5.0, double b = 5.0)
		: TestFunc(aNew, bNew, a, b)
	{  // [a, b] - "Родные" границы изменения аргумента
		paramGeneration();
		scanTestFunction();
	}
	virtual	void paramGeneration()
	{
		xGlobMin = getRandomIn(Fa, Fb);
		//@ cout << "xGlobMin = " << xGlobMin << " - real coordinate" << endl;
	}
	double calcTestFunc(double x)
	{
		double PinterFunc;
		PinterFunc = 0.0025*(x - xGlobMin)*(x - xGlobMin)
			+ sin(x - xGlobMin)*sin(x - xGlobMin)
			+ sin(x - xGlobMin + (x - xGlobMin)*(x - xGlobMin))
			* sin(x - xGlobMin + (x - xGlobMin)*(x - xGlobMin));
		return PinterFunc;
	}
};
#endif // PINTERTASK_H
