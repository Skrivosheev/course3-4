﻿#ifndef INTERVAL_H    
#define INTERVAL_H

#include <vector>
#include "TProblem.h"

class interval
{
public:
	double left;
	double right;
	double dx;
	double x_check;
	double x_correct;

	double z_check;

	std::vector<double> z_left;
	std::vector<double> z_right;

	double left_internal_x;
	double right_internal_x;

	std::vector<double> c; // у второго метода мб хранить
	std::vector<double> w; // не надо хранить 

	std::vector<double> local_mark_lagrange;
	std::vector<double> local_mark_max;
	std::vector<double> local_mark_careful;
	std::vector<double> local_l;
	std::vector<double> mixed_mark;
	std::vector<double> global_mark;

	double characteristic_r;
	TProblem *_p;

	interval (const double left_border, const double right_border,TProblem *p)
	{
		left = left_border;
		right = right_border;
		dx = right - left;
		_p = p;

		calc_values_constrains();
	}

	void get_psi()
	{
		for(auto i=0;i<=_p->getConstraintsCount();i++)
		{
			auto tmp_w = (pow(right, 2) - pow(left, 2) - 2 * (z_right[i] + z_left[i]))/(2*(right-left)*mixed_mark[i]);
			w.push_back(tmp_w);

			auto tmp_c = (left + right - mixed_mark[i] / 2 * (pow(left - tmp_w, 2) + pow(right - tmp_w, 2)))/2;
			c.push_back(tmp_c);
		}
	}

	double get_psi_for_f(const double x)
	{
		const auto tmp_psi = c[0] + mixed_mark[0] / 2 * pow(x - w[0],2);
		return tmp_psi;
	}

	void get_internal_interval()
	{
		left_internal_x = left;
		right_internal_x = right;

		for (auto i=1;i<=_p->getConstraintsCount();i++)
		{
			const auto sqrt_value = sqrt(-c[i] * 2 / mixed_mark[i]);

			const auto tmp_left = sqrt_value + w[i];
			if (left_internal_x > tmp_left)
			{
				left_internal_x = tmp_left;
			}

			const auto tmp_right = -sqrt_value + w[i];
			if(right_internal_x<tmp_right)
			{
				right_internal_x = tmp_right;
			}
			
		}
	}

	void get_x_correction(const double nu)
	{
		if(w[0]<left+nu*dx)
		{
			x_correct = left + nu * dx;
		} 
		else if(left+nu*dx<=w[0] && w[0]<=left-nu*dx)
		{
			x_correct = w[0];
		}
		else if(w[0]>right-nu*dx)
		{
			x_correct = right - nu * dx;
		}
	}

	void set_x_check(const double x)
	{
		x_check = x;
	}

	void calc_x_check()
	{
		if(x_correct<left_internal_x)
		{
			x_check = left_internal_x;
			return;
		}
		if(x_correct>right_internal_x)
		{
			x_check = right_internal_x;
			return;
		}
		x_check = x_correct;
	}

	void calc_characteristic()
	{
		get_internal_interval();
		if(right_internal_x-left_internal_x<0)
		{
			characteristic_r = std::numeric_limits<double>::max();
			return;
		}
		if(x_correct<left_internal_x)
		{
			characteristic_r = get_psi_for_f(left_internal_x);
			return;
		}
		if(x_correct>=right_internal_x)
		{
			characteristic_r = get_psi_for_f(right_internal_x);
			return;
		}
		characteristic_r = get_psi_for_f(x_correct);
	}

private:
	void calc_values_constrains()
	{
		for (auto i=0; i <= _p->getConstraintsCount(); ++i)
		{
			z_left.push_back((*_p)(left, i));
			z_right.push_back((*_p)(right, i));
		}
	}
};

#endif INTERVAL_H