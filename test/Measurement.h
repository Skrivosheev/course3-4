#ifndef MEASUREMENT_H    
#define MEASUREMENT_H

struct Measurement
{
	double x_point;
	double y_point;

	Measurement(double x_point_, double y_point_)
	{
		x_point = x_point_;
		y_point = y_point_;
	}
};

#endif MEASUREMENT_H
