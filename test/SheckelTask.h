﻿#ifndef SHECKELTASK_H
#define SHECKELTASK_H

#include "TestFunc.h"

class SheckelTask : public TestFunc {
	double parC[10], parK[10], parA[10];
public:
	SheckelTask(double aNew, double bNew, double a = 0.0, double b = 10.0)
		: TestFunc(aNew, bNew, a, b)
	{  // [a, b] - "Родные" границы изменения аргумента
		paramGeneration();
		scanTestFunction();
	}
	virtual	void paramGeneration()
	{
		for (int i = 0; i < 10; ++i)
		{
			parC[i] = getRandomIn(0.001, 2.0);
			parK[i] = getRandomIn(3.0, 10.0);
			parA[i] = getRandomIn(0.0, 10.0);
		}
	}
	double calcTestFunc(double x)
	{
		double SheckelFunc{ 0.0 };

		for (int i = 0; i < 10; i++)
			SheckelFunc += -1.0 / (parC[i] + (x - parA[i])*(x - parA[i]));
		return SheckelFunc;
	}
};
#endif // SHECKELTASK_H
