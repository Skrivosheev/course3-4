﻿#ifndef TESTFUNC_H
#define TESTFUNC_H

#include <vector>
#include <iostream>
#include <stdlib.h>
using namespace std;

enum class TypeProblem { AnyTask, TestClass };
enum class TestFuncType { Pinter, Sheckel, Gibson, Polinom };

using TGlobalSolution = struct Point { double x; double y; };

class TProblem;

class TestFunc
{
protected:
	double Fa, Fb;//Принятый диапазон определения данной тестовой функции
	double FaNew, FbNew;//Диапазон, к которому функция должна быть приведена
	int pointsCount{ 1000 };//Число точек в сетке при грубом определении Min и Max
	double epsX{ 1.0e-06 }; //Точность определения глобальных минимумов по х
	double epsY{ 1.0e-04 }; //Точность сравнения значений в глоб.минимумах
	vector<TGlobalSolution> globalSolutions; //Набор глобальных минимумов тестовой задачи
	double minFunc, maxFunc;//Границы колебания функции
	//Временное хранилище грубых оценок локальных минимумов тестовой функции:
	static vector<TGlobalSolution> localTmpSolutions;
	static vector<unsigned int> Fib;//Числа Фибоначчи
public:
	TestFunc(double aNew, double bNew, double a, double b)
		: Fa(a), Fb(b), FaNew(aNew), FbNew(bNew)
	{
		generateFib();
		/*if (!Fib.size())
		{ Fib.push_back(1); Fib.push_back(1);
		for (int i = 2; i < 46; ++i)
			Fib.push_back(Fib[i - 2] + Fib[i - 1]);
		}*/
	}
	virtual void paramGeneration() = 0; //Генерация параметров функции

	virtual double calcTestFunc(double x) = 0; //Вычисление функции в "родных" координатах

	double operator()(double x)//Вычисление функции в координатах из [FaNew, FbNew]
	{
		auto xx = Fa + (Fb - Fa)*(x - FaNew) / (FbNew - FaNew);
		return calcTestFunc(xx);
	}

	//Определение максимального и минимального значений, 
	// формирование списк точно найденных глобальных минимумов:

	void scanTestFunction();

	double getMin() { return minFunc; }

	double getMax() { return maxFunc; }

	int globalSolutionsCount()
	{
		return (int)globalSolutions.size();
	}

	TGlobalSolution operator[](int i)
	{
		i = i < 0 ? 0 : i;
		i = (int)((size_t)i >= globalSolutions.size() ? globalSolutions.size() - 1 : i);
		return globalSolutions[i];
	}

	//Поиск минимума на [x0, x1] из [0, 1] методом Фибоначчи с абсолютной точностью epsXabs
	// у функции (*obj)(x), при obj != NULL или (*obj2)(x) при obj2 != NULL;
	// Результат помещается по ссылке в структуру p:

	static	void getLocalMin(double x0, double x1, double epsXabs,
		TestFunc* obj, TProblem* obj2, TGlobalSolution & p);

	static	double getRandomIn(double a, double b)
	{
		auto per = (double)rand() / (double)RAND_MAX;
		auto r = a + per * (b - a);
		return r;
	}

	static void generateFib()
	{
		if (!Fib.size())
		{
			Fib.push_back(1); Fib.push_back(1);
			for (auto i = 2; i < 46; ++i)
				Fib.push_back(Fib[i - 2] + Fib[i - 1]);
		}
	}

};
#endif // TESTFUNC_H
