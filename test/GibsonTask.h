﻿#ifndef GIBSONTASK_H
#define GIBSONTASK_H

#include <corecrt_math_defines.h>
#include "TestFunc.h"

class GibsonTask : public TestFunc {
	double parA[14], parB[14], par{ 0.0 };
public:
	GibsonTask(double aNew, double bNew, double a = 0.0, double b = 1.0)
		: TestFunc(aNew, bNew, a, b)
	{  // [a, b] - "Родные" границы изменения аргумента
		paramGeneration();
		scanTestFunction();
	}
	virtual	void paramGeneration()
	{
		for (int i = 0; i < 14; ++i)
		{
			parA[i] = getRandomIn(0.0, 1.0);
			parB[i] = getRandomIn(0.0, 1.0);
		}
	}
	double calcTestFunc(double x)
	{
		double GibsonFunc{ 0.0 };
		for (int i = 0; i < 14; i++)
			GibsonFunc += parA[i] * sin(4 * M_PI*x*i) + parB[i] * cos(4 * M_PI*x*i);
		return GibsonFunc;
	}
};
#endif // GIBSONTASK_H
