﻿
#include "pch.h"
#include "TProblem.h"
#include "GibsonTask.h"
#include "PinterTask.h"
#include "SheckelTask.h"
#include "PolinomTask.h"
#include <algorithm>

//Временное хранилище грубых оценок условных локальных минимумов тестовой задачи:
vector<TGlobalSolution> TProblem::localTmpSolutions;

// Выч-е функции из встроенного набора:
double TProblem::anyFunction(const double x, const int numStFunc)
{
	auto y = 0.0;
	switch (numStFunc)
	{
	case 0:
		y = (x + 2.0)*x*(x - 2.0); break;
	case 1:
		y = (x - 0.5)*(x + 1.0); break;
	default:
		y = x;
	}
	return y;
}
//Инициализация встроенной задачи:
void TProblem::initializeTask()
{
	taskFunctions.clear();
	switch (numberTask)
	{
	case 0:
		taskFunctions = { 0,1 };
		Fa = -3; Fb = 2.0; break;
	default:
		taskFunctions = { 0,1 };
		Fa = -3.5; Fb = 2.5; break;

	}
	constraintsCount = (int)(taskFunctions.size() - 1);
}

//Фабрика функций тестовой задачи:
//Первый вызов должен быть при isConstraints == false
// при isConstraints == false в поля Fa, Fb задачи должны помещаться значения
// "родных" границ минимизируемой функции,
// при последующих вызовах для генерации функций ограничений (при isConstraints == true)
// конструкторы этих функций должны вызываться со значениями границ Fa, Fb,
// взятых из полей генерируемой задачи
TestFunc* TProblem::generateTestFunction(TestFuncType funcType, bool isConstraints, int rootCount)
{
	switch (funcType)
	{
	case TestFuncType::Pinter:
		if (!isConstraints) { Fa = -5.0; Fb = 5.0; }
		return static_cast<TestFunc *>(new PinterTask(Fa, Fb));

	case TestFuncType::Sheckel:
		if (!isConstraints) { Fa = 0.0; Fb = 10.0; }
		return static_cast<TestFunc *>(new SheckelTask(Fa, Fb));

	case TestFuncType::Gibson:
		if (!isConstraints) { Fa = 0.0; Fb = 1.0; }
		return static_cast<TestFunc *>(new GibsonTask(0.0, 1.0));

	case TestFuncType::Polinom:
		if (!isConstraints) { Fa = 0.0; Fb = 1.0; }
		return static_cast<TestFunc *>(new PolinomTask(Fa, Fb, rootCount));
	default:
		if (!isConstraints) { Fa = 0.0; Fb = 1.0; }
		return static_cast<TestFunc *>(new PolinomTask(Fa, Fb, rootCount));
	}

}
void TProblem::generateTestTask()//Генерация тестовой задачи
{
	const unsigned int start_rnd = 10 * (int(typeMainConstraint) + 1)
		+ 100 * (int(typeMinFunc) + 1)
		+ 10000 * numberTask + 1000 * constraintsCount;
	srand(start_rnd);

	testFunctions.clear();
	constraintsValues.clear();
	//Минимизируемая функция:
	testFunctions.push_back(generateTestFunction(typeMinFunc, false, 0));
	constraintsValues.push_back(0);
	//Первое ограничение:
	testFunctions.push_back(generateTestFunction(TestFuncType::Polinom, true, 5));
	// Установка значения верхнего ограничителя
	constraintsValues.push_back(getConstraintValue(1, alpha));
	//Главное ограничение:
	testFunctions.push_back(generateTestFunction(typeMainConstraint, true, 5));
	// Установка значения верхнего ограничителя
	constraintsValues.push_back(getConstraintValue(2, alpha));
	//Дополнительные ограничения:
	for (int i = 3; i <= constraintsCount; ++i)
	{ 	//Дополнительное полиномиальное ограничение:
		testFunctions.push_back(generateTestFunction(TestFuncType::Polinom, true, 6));
		// Установка значения верхнего ограничителя
		constraintsValues.push_back(getConstraintValue(i, alpha));
	}
}
//Приближенное определение доли допустимых точек на сетке из M точек:
double TProblem::getProportionGoodDomain(int M)
{
	int goodPointCount = 0;
	for (int i = 0; i <= M; ++i)
	{
		double x = i * 1.0 / (double)M, y;
		bool dop = true;
		for (int j = 0; j <= getConstraintsCount(); ++j)
		{
			y = (*this)(x, j);
			if (j > 0 && y > 0) dop = false;
		}
		if (dop) ++goodPointCount;
	}
	return (double)goodPointCount / (double)M;
}
//Подбор уровня отсечения Alpha для достижения меры mu допустимого множества
// (вернет достигнутое приближение mu)
double TProblem::seachAlphaValue()
{
	double alphR = 1.0, alphL = 1.0, alphStep = 0.01, alphC;
	double muR = 1.0, muL, muC;
	do {
		alphL -= alphStep;
		setNewAlphaValue(alphL);
		muL = getProportionGoodDomain(tmpPointCount);
		alphStep *= 1.2;
	} while (muL > mu);
	do {
		alphC = 0.5*(alphL + alphR);
		setNewAlphaValue(alphC);
		muC = getProportionGoodDomain(tmpPointCount);
		if (muC < mu) { alphL = alphC; muL = muC; }
		else if (muC == mu) { alpha = alphC; return mu; }
		else { alphR = alphC;   muR = muC; }
	} while (fabs(muC - mu) > 2.0 / tmpPointCount);
	if (muR - mu < mu - muL) { alpha = alphR; return muR; }
	else { alpha = alphL; return muL; }
}

//Определение максимального и минимального условных значений, 
// формирование списка точно найденных условных глобальных минимумов тестовой задачи.
// Поиск проводится в нормированных координатах, список глобальных минимумов заполняется
// в нормированных координатах:
void TProblem::scanTestTask()
{
	localTmpSolutions.clear();
	globalSolutions.clear();
	auto FaNew = 0.0, FbNew = 1.0; //Границы нормированного отрезка
	double x;
	double y;
	double g;
	double xNext;
	double yNext;
	double gNext;
	auto h = (FbNew - FaNew) / (double)(pointsCount - 1);
	double min_x;
	auto goodPointExist = false; //Встретилась ли допустимая точка?
	auto yPred = (*this)(FaNew, 0);
	auto gPred = G(FaNew);
	if (gPred <= 0)
	{
		minFunc = maxFunc = yPred;
		min_x = FaNew;
		goodPointExist = true;
	}

	//@	cout << "-----------Set points:" << endl;
	//@	cout << "x = " << FaNew << " y = " << yPred << endl;
	x = FaNew + h;
	y = (*this)(x, 0);
	g = G(x);
	if (g <= 0)
		if (!goodPointExist)
		{
			minFunc = maxFunc = yPred;
			min_x = x;
			goodPointExist = true;
		}
		else
		{
			if (minFunc > y) min_x = x;
			minFunc = min<double>(minFunc, y);
			maxFunc = max<double>(maxFunc, y);
		}
	//@	cout << "x = " << x << " y = " << y << endl;
	//Ищем все локальные "ямки"
	TGlobalSolution p;
	if (gPred <= 0.0 && (yPred < y && g <= 0 || g > 0))
	{
		p.x = FaNew; p.y = yPred;
		localTmpSolutions.push_back(p);
	}
	for (int i = 2; i <= pointsCount - 1; i++)
	{
		xNext = FaNew + h * i;
		yNext = (*this)(xNext, 0);
		gNext = G(xNext);
		//@ cout << "x = " << xNext << " y = " << yNext << endl;
		if (gNext <= 0)
			if (!goodPointExist)
			{
				minFunc = maxFunc = yNext;
				min_x = xNext;
				goodPointExist = true;
			}
			else
			{
				if (minFunc > yNext) min_x = xNext;
				minFunc = min<double>(minFunc, yNext);
				maxFunc = max<double>(maxFunc, yNext);
			}
		if (g <= 0.0)
			if ((gPred <= 0.0 && yPred > y || gPred > 0.0)
				&& (y < yNext && gNext <= 0.0 || gNext > 0.0))
			{
				p.x = x; p.y = y;
				localTmpSolutions.push_back(p);
			}

		if (gNext <= 0.0)
			if ((y > yNext && g <= 0.0 || g > 0.0) && i == pointsCount - 1)
			{
				p.x = xNext; p.y = yNext;
				localTmpSolutions.push_back(p);
			}
		yPred = y; gPred = g; x = xNext; y = yNext; g = gNext;
	}
	//@	cout << "-----------Set local points:" << endl;
	//@	for (int j = 0; j < (int)localTmpSolutions.size(); ++j)
	//@		cout << "x = " << localTmpSolutions[j].x
	//@		<< " y = " << localTmpSolutions[j].y << endl;

	double porogFunc = minFunc + (maxFunc - minFunc) / 10;
	//Уточнение локальных минимумов, выделение глобальных:
	for (int i = 0; i < (int)localTmpSolutions.size(); ++i)
	{
		if (localTmpSolutions[i].y > porogFunc) continue;
		double x0, x1;
		x0 = localTmpSolutions[i].x - h;
		x1 = x0 + 2 * h;
		x0 = max<double>(FaNew, x0);
		x1 = min<double>(FbNew, x1);
		anyLocalSolution = localTmpSolutions[i];
		//Уточняем условный локальный минимум методом Фибоначчи:
		TestFunc::getLocalMin(x0, x1, (FbNew - FaNew)*epsX, nullptr, this, p);

		if (minFunc > p.y) minFunc = p.y;
		if (p.y < minFunc + epsY * (maxFunc - minFunc))
			globalSolutions.push_back(p);
	}

	int count = (int)globalSolutions.size(), minusCount = 0;
	for (int i = 0; i < (int)globalSolutions.size() - minusCount; ++i)
		if (globalSolutions[i].y > minFunc + epsY * (maxFunc - minFunc))
		{
			globalSolutions[i] = globalSolutions[count - 1 - minusCount];
			minusCount++;
		}
	globalSolutions.resize(count - minusCount);
}