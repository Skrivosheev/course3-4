﻿#ifndef PARABOLASMETHOD_H    
#define PARABOLASMETHOD_H

#include "Methods.h"
#include <list>
#include "Interval.h"
#include "TProblem.h"
#include "Measurement.h"

class parabolas_method : methods
{
	double Fa;
	double Fb;
	double Fmid;
	TProblem *P;
	double v_; // 0<v<<1
	double betta_; // доля от длины исходного интервала при расчете лок смеш оценки
	double delta_;//параметр метода
	double gamma_loc;//параметр метода
	int number_; // номер задачи
	double gamma_; //параметр метода
	int n_; //параметр метода

	interval* help;

	int count_measurements;
	double degree_of_marks;
	double epsX{ 1.0e-06 }; //Точность определения глобальных минимумов по х
	double epsY{ 1.0e-04 }; //Точность сравнения значений в глоб.минимумах

	int step_{ 0 };
	int max_steps_;

	std::list<interval*> intervals_List;
	std::vector<Measurement*> measurements;

	bool flag_for_reload_marks_;

public:
	parabolas_method(double v, 
		double betta, double delta, double gamma_loc_,double gamma, int max_steps,int n, TProblem *test):
		number_(0), help(nullptr),
		flag_for_reload_marks_(false)
	{
		count_measurements = 0;
		v_ = v;
		betta_ = betta;
		delta_ = delta;
		gamma_loc = gamma_loc_;
		gamma_ = gamma;
		max_steps_ = max_steps;
		n_ = n;
		P = test;
		Fa = test->left();
		Fb = test->right();
		degree_of_marks = 0;
		Fmid = (Fa + Fb) / 2;
	}

	void print_meas()
	{
		for (auto i=0;i<count_measurements;i++)
		{
			cout << "x_value [" << i << "]= " << measurements[i]->x_point
				<< "      y_value [" << i << "]= " << measurements[i]->y_point << endl;
		}
	}

	void run_method()
	{
		do_step_method_first();
		while (true)
		{
			do_step_method();
			if(help->dx <= epsX || count_measurements==max_steps_)
			{
				break;
			}
		}
	}

	void do_step_method_first()
	{
		auto added1 = new interval(Fa, Fmid, P);
		auto added2 = new interval(Fmid, Fb, P);

		const auto a = create_measurement(Fa);
		const auto mid = create_measurement(Fmid);
		const auto b = create_measurement(Fb);

		measurements.push_back(a);
		measurements.push_back(mid);
		measurements.push_back(b);
		count_measurements += 3;

		intervals_List.push_back(added1);
		intervals_List.push_back(added2);

		for (auto i = 0; i <= P->getConstraintsCount(); ++i)
		{
			auto lagrange_mark = abs(lagrange_polynomial(added1->left, Fmid, added2->right,
				added1->z_left[i], (*P)(Fmid, i), added2->z_right[i]));

			added1->local_mark_lagrange.push_back(lagrange_mark);
			added2->local_mark_lagrange.push_back(lagrange_mark);
		}

		added1 = get_marks(added1);
		added1->get_psi();
		added1->get_x_correction(v_);
		added1->calc_characteristic();

		added2 = get_marks(added2);
		added2->get_psi();
		added2->get_x_correction(v_);
		added2->calc_characteristic();

		help = find_max_r();

		for (auto finder = intervals_List.begin(); finder != intervals_List.end(); ++finder)
		{
			if (*finder==help)
			{
				(*finder)->calc_x_check();
				const auto meas = create_measurement((*finder)->x_check);
				measurements.push_back(meas);
				count_measurements++;
				intervals_List.erase(finder);
				break;
			}
		}

		step_++;
	}

	void do_step_method()
	{			
		auto added1 = new interval(help->left,help->x_check, P);
		auto added2 = new interval(help->x_check, help->right, P);

		add_interval_at_list(added1);
		add_interval_at_list(added2);

		for (auto i = 0; i <= P->getConstraintsCount(); ++i)
		{
			auto lagrange_mark = abs(lagrange_polynomial(added1->left, help->x_check, added2->right,
				added1->z_left[i], (*P)(help->x_check, i), added2->z_right[i]));

			added1->local_mark_lagrange.push_back(lagrange_mark);
			added2->local_mark_lagrange.push_back(lagrange_mark);
		}

		added1 = get_marks(added1);
		added2 = get_marks(added2);

		reload_marks();

		added1->get_psi();
		added1->get_x_correction(v_);
		added2->get_psi();
		added2->get_x_correction(v_);

		added1->calc_characteristic();
		added2->calc_characteristic();

		help = find_max_r();

		for(auto finder = intervals_List.begin(); finder != intervals_List.end(); ++finder)
		{
			if (*finder == help)
			{
				(*finder)->calc_x_check();
				const auto meas=create_measurement((*finder)->x_check);
				measurements.push_back(meas);
				count_measurements++;
				intervals_List.erase(finder);
				break;
			}
		}

		step_++;
	}

	void reload_marks()
	{
		for (auto i = 0; i < P->getConstraintsCount(); i++)
		{
			for (auto finder = intervals_List.begin(); finder != intervals_List.end(); ++finder)
			{
				if (finder == intervals_List.begin())
				{
					auto tmp1 = finder;
					auto tmp = tmp1++;
					(*finder)->mixed_mark[i] = max((*tmp)->mixed_mark[i], (*tmp1)->mixed_mark[i]);
				}
				else if (finder == --intervals_List.end())
				{
					auto tmp1 = finder;
					auto tmp = tmp1--;
					(*finder)->mixed_mark[i] = max((*tmp)->mixed_mark[i], (*tmp1)->mixed_mark[i]);
				}
				else
				{
					auto tmp1 = finder;
					auto tmp = tmp1++;
					auto tmp2 = finder;
					--tmp2;
					(*finder)->mixed_mark[i] = max(max((*finder)->mixed_mark[i], (*tmp)->mixed_mark[i]),(*tmp2)->mixed_mark[i]);
				}
			}
		}
	}

	interval* get_marks(interval* marked_interval)
	{
		auto max_mark_l = get_max_local_mark();
		auto local_mark_careful = get_local_mark_careful();

		for (auto i = 0; i <= P->getConstraintsCount(); ++i)
		{
			marked_interval->local_mark_max.push_back(max_mark_l[i]);

			if (max_mark_l[i] > delta_)
			{
				marked_interval->local_l.push_back(gamma_loc * local_mark_careful[i]);
			}
			else if (max_mark_l[i] == delta_)
			{
				marked_interval->local_l.push_back(delta_);
			}
			else marked_interval->local_l.push_back(local_mark_careful[i]);

			if(step_<n_)
			{
				if (max_mark_l[i] > 0)
				{
					marked_interval->global_mark.push_back(gamma_*max_mark_l[i]);
				} else //if (max_mark_l[i]==0)
				{
					marked_interval->global_mark.push_back(1);
				}

			}

			else if(step_==n_)
			{
				if (marked_interval->local_mark_max[i] > 0)
				{
					degree_of_marks = marked_interval->local_mark_max[i] * (gamma_ - 1);
				} 
				else
				{
					degree_of_marks = 1;
				}
			}

			else if(step_>=n_)
			{
				if (max_mark_l[i] > 0)
				{
					marked_interval->global_mark.push_back(max_mark_l[i] + degree_of_marks);
				}
				else if(max_mark_l[i]==0)
				{
					marked_interval->global_mark.push_back(1);
				}
			}
		}

		const auto dx = marked_interval->dx;
		const auto param_d = betta_ * (P->right() - P->left());

		for (auto i = 0; i <= P->getConstraintsCount(); ++i)
		{
			if (dx <= param_d)
			{
				const auto tmp = marked_interval->global_mark[i] * (dx / param_d) + (1 - dx / param_d)*marked_interval->local_l[i];
				marked_interval->mixed_mark.push_back(tmp);
			}
			else
			{
				marked_interval->mixed_mark.push_back(marked_interval->global_mark[i]);
			}
		}
		return marked_interval;
	}

	std::vector<double> get_local_mark_careful()
	{
		const auto size = P->getConstraintsCount();
		std::vector<double> careful_marks;

		auto start_it = intervals_List.begin();
		auto start_it_help = intervals_List.begin();
		auto mid_it = intervals_List.begin();
		auto end_it = intervals_List.end();
		--end_it;

		if(step_==0)
		{
			for (auto i=0;i<=size;i++)
			{
				auto tmp = max((*start_it)->local_mark_lagrange[i], (*end_it)->local_mark_lagrange[i]);
				careful_marks.push_back(max(tmp,delta_));
			}
		} 
		else if(step_>=1 && step_!=max_steps_-1)
		{
			advance(start_it, step_+1);
			advance(mid_it, step_);
			advance(start_it_help, step_-1);

			for (auto i=0;i<=size;i++)
			{
				auto careful_mark = max((*start_it)->local_mark_lagrange[i], (*mid_it)->local_mark_lagrange[i]);
				careful_marks.push_back(max(max((*start_it_help)->local_mark_lagrange[i], careful_mark),delta_));
			}
		}
		else if(step_==max_steps_-1)
		{
			advance(start_it, step_);
			advance(mid_it, step_ + 1);

			for (auto i = 0; i <= size; i++)
			{
				auto careful_mark = max((*start_it)->local_mark_lagrange[i], (*mid_it)->local_mark_lagrange[i]);
				careful_marks.push_back(max(careful_mark, delta_));
			}
		}

		return  careful_marks;
	}

	std::vector<double> get_max_local_mark()
	{
		std::vector<double> max_l;

		for (auto i = 0; i <= P->getConstraintsCount(); i++)
		{
			max_l.push_back((*intervals_List.begin())->local_mark_lagrange[i]);
			for (auto finder = intervals_List.begin(); finder != intervals_List.end(); ++finder)
			{

				if ((*finder)->local_mark_lagrange[i] > max_l[i])
				{
					max_l[i] = (*finder)->local_mark_lagrange[i];
				}
			}
		}

		return max_l;
	}

	Measurement* create_measurement(const double x) const
	{
		const auto z = (*P)(x,0);
		const auto return_meas = new Measurement(x, z);
		return return_meas;
	}

	bool check_stop_condition(const double x1, const double x2) const
	{
		if(step_==max_steps_ || x2-x1<=epsX)
		{
			return true;
		}
		return false;
	}

	int count_step() const
	{
		return step_;
	}

	interval* find_max_r()
	{
		double max_r=0;
		auto number = *intervals_List.begin();
		for (auto finder = intervals_List.begin(); finder != intervals_List.end(); ++finder)
		{
			if((*finder)->characteristic_r>max_r)
			{
				number = *finder;
				max_r = (*finder)->characteristic_r;
			}
		}
		return number;
	}

	static double lagrange_polynomial(const double x1, const double x2, const double x3, const double y1, const double y2, const double y3)
	{
		const auto n = 3;
		double x[n] = { x1,x2,x3 };
		double y[n] = { y1,y2,y3 };
		double z = 0;
		for (auto j = 0; j < n; j++)
		{
			double p1 = 1; double p2 = 1;
			for (auto i = 0; i < n; i++)
			{
				if (i == j) {
					p1 = p1 * 1; p2 = p2 * 1;
				}
				else {
					p1 = 1;
					p2 = p2 * (x[j] - x[i]);
				}
			}
			z = z + y[j] * 2 * p1 / p2;
		}
		return z;
	}

	void add_interval_at_list(interval* additional)
	{
		for(auto finder=intervals_List.begin();finder!=intervals_List.end();++finder)
		{
			if (additional->left <= (*finder)->left)
			{
				intervals_List.insert(finder, additional);
				break;
			}
		}
	}
};


#endif PARABOLASMETHOD_H
